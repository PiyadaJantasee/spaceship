using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public GameObject BulletPrefab;
    public float BulletSpeed;
    private bool fired = false;
    private float ShootTimer = 1;
    public float MovementSpeed;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(0,-MovementSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        ShootTimer = ShootTimer - Time.deltaTime;
        if (ShootTimer < 0 && fired == false)
        {
            fired = true;
            GameObject BulletInstance = Instantiate(BulletPrefab);
            BulletInstance.transform.SetParent(transform.parent);
            BulletInstance.transform.position =
                new Vector3(transform.position.x, transform.position.y - 0.8f, transform.position.z);
            BulletInstance.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -BulletSpeed, transform.position.z);
            Destroy(BulletInstance.gameObject, 4);
            ShootTimer = 2;
        }
        else
        {
            fired = false;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
        {
            if(other.tag == "PlayerBullet");
            {
                Gamecontroller.sp.AddScore();
                Destroy(gameObject);
                Destroy(other.gameObject);
            }
        }
        
    }

