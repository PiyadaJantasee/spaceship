using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Gamecontroller : MonoBehaviour
{
    public float HorizontalScreenLimit;

    public static Gamecontroller sp;
    // enemy
    public GameObject EnemyPrefab;

    float EnemySpawnTime = 4;

    public GameObject Player;
    //score
    public TextMeshProUGUI ScoreUI;
    private int ScoreCounter = 0;
    //highscore
    public TextMeshProUGUI HighscoreUi;
    //game over
    public TextMeshProUGUI GameOverHighScoreUI;
    public GameObject GameOverPanel;
    public TextMeshProUGUI GameOverScoreUi;
    // Timer
    public TextMeshProUGUI PotionUI;
    public float PotionTimer = 100;
    // Start is called before the first frame update
    void Start()
    {
        sp = this;
        HighscoreUi.text = "HighScore:" + PlayerPrefs.GetInt("HighScoreVal");
    }

    // Update is called once per frame
    void Update()
    {
        EnemySpawnTime = EnemySpawnTime - Time.deltaTime;
        if (EnemySpawnTime < 0 && Player != null)
        {
            GameObject EnemyObject = Instantiate(EnemyPrefab);
            EnemyObject.transform.SetParent(transform);
            EnemyObject.transform.position = new Vector3(Random.Range(HorizontalScreenLimit, -HorizontalScreenLimit),
                Player.transform.position.y + 8, Player.transform.position.z);
            EnemySpawnTime = Random.Range(1, 4);
            Destroy(EnemyObject.gameObject, 10);
        }

        // timer
        if (Player != null)
        {
            PotionTimer = PotionTimer - Time.deltaTime * 5;
            PotionUI.text = "Time: " + PotionTimer.ToString("0");
            if (PotionTimer < 0)
            {
                CheckHighScore();
                Destroy(Player.gameObject);
            }
        }
    }
    //score
    public void AddScore()
    {
        ScoreCounter = ScoreCounter + 1;
        ScoreUI.text = "Score:" + ScoreCounter;
    }

    //high score  
    public void CheckHighScore()
    {
        StartCoroutine(AddTimerVadidateHighscore());
    }

    IEnumerator AddTimerVadidateHighscore()
    {
        yield return new WaitForSeconds(1);
        if (ScoreCounter > PlayerPrefs.GetInt("HighScoreVal"))
        {
            PlayerPrefs.SetInt("HighScoreVal", ScoreCounter);
            HighscoreUi.text = "HighScore:" + ScoreCounter;
        }
        else
        {
            HighscoreUi.text = "HighScore:" + PlayerPrefs.GetInt("HighScoreVal");
        }

        GameOverProcess();
    }
    public void GameOverProcess()
    {
        GameOverPanel.SetActive(true);
        GameOverHighScoreUI.text = "" + PlayerPrefs.GetInt("HighScoreVal");
        GameOverScoreUi.text = "" + ScoreCounter;
    }
    //restart game
    public void RestartGame()
    {
        SceneManager.LoadScene("Game2");
    }
}

