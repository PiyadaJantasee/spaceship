using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playercontrol : MonoBehaviour
{
    public float PlayerMovementSpeed;

    public float PlayerVerticalMoveSpeed = 3;

    public GameObject BulletPrefab;

    public float BulletSpeed;
    public bool fired = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        GetComponent<Rigidbody2D>().velocity =
            new Vector2(Input.GetAxis("Horizontal") * PlayerMovementSpeed, PlayerVerticalMoveSpeed);
        //right
        if (transform.position.x > Gamecontroller.sp.HorizontalScreenLimit)
        {
            transform.position = new Vector2(Gamecontroller.sp.HorizontalScreenLimit, transform.position.y);
        }

        //Left
        if (transform.position.x < -Gamecontroller.sp.HorizontalScreenLimit)
        {
            transform.position = new Vector2(-Gamecontroller.sp.HorizontalScreenLimit, transform.position.y);
        }
        //bullet
        if (Input.GetAxis("Fire1") == 1)
        {
            if (fired == false)
            {
                fired = true;
                GameObject BulletInstance = Instantiate(BulletPrefab);
                BulletInstance.transform.SetParent(transform.parent);
                BulletInstance.transform.position = new Vector3(transform.position.x,transform.position.y + 0.8f,transform.position.z);
                BulletInstance.GetComponent<Rigidbody2D>().velocity = new Vector3(0, BulletSpeed, transform.position.z);
                Destroy(BulletInstance.gameObject,5);
            }
        }
        else
        {
            fired = false;
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "EnemyBullet");
        {
            Gamecontroller.sp.CheckHighScore();
            Destroy(gameObject);
            Destroy(other.gameObject);
        }
       
    }
}
    

