using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager sp;
    [SerializeField] public AudioSource GameSource;

    private void Start()
    {
        sp = this;
        GameSource.Play();
    }

    private void FixedUpdate()
    {
        
    }

    public void stop()
    {
        GameSource.Stop();
    }
}

